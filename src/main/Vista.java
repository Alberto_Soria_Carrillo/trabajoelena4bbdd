package main;

import com.github.lgooddatepicker.components.DateTimePicker;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

public class Vista {
    private JPanel panel1;
    JTextField txtMatricula;
    JTextField txtMarca;
    JTextField txtModelo;
    JButton btnBuscar;
    JButton btnNuevo;
    JButton btnEliminar;
    JTextField txtBuscar;
    JTable tabla;
    DateTimePicker dateTimePicker;
    JLabel lblAccion;

    DefaultTableModel dtm;
    JMenuItem itemConectar;
    JMenuItem itemSalir;
    JMenuItem itemCrearTabla;
    JFrame frame;

    public Vista() {
        frame = new JFrame("Vehiculo BBDD");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        dtm =new DefaultTableModel();
        tabla.setModel(dtm);

        crearMenu();

        frame.pack();
        frame.setVisible(true);

    }
    private void crearMenu() {
        itemConectar = new JMenuItem("Conectar");
        itemConectar.setActionCommand("Conectar");
        itemCrearTabla = new JMenuItem("Crear tabla coches");
        itemCrearTabla.setActionCommand("CrearTablaCoches");
        itemSalir = new JMenuItem("Salir");
        itemSalir.setActionCommand("Salir");

        JMenu menuArchivo = new JMenu("Archivo");
        menuArchivo.add(itemConectar);
        menuArchivo.add(itemCrearTabla);
        menuArchivo.add(itemSalir);
        JMenuBar barraMenu = new JMenuBar();
        barraMenu.add(menuArchivo);

        frame.setJMenuBar(barraMenu);
    }
}
