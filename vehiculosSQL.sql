-- Creacion de la BBDD y su tablas
CREATE DATABASE vehiculos;

use vehiculos;

CREATE TABLE coches(
id int primary key auto_increment,

matricula varchar(30) unique,

marca varchar (40),

modelo varchar (40),

fecha_matriculacion timestamp);
-- Insercion de datos
INSERT INTO coches(matricula, marca, modelo, fecha_matriculacion)

VALUES('FWD-1234', 'Peugeot', '507','2017-05-06');
-- Comprobacion de la informacion
select * from coches;
-- Creacion del procedimiento para la creacion de tablas no creadas
DELIMITER //
CREATE PROCEDURE crearTablaCoches()
BEGIN
    CREATE TABLE coches(
                           id int primary key auto_increment,
                           matricula varchar(30) unique,
                           marca varchar (40),
                           modelo varchar (40),
                           fecha_matriculacion timestamp);
END //